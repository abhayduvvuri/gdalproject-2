const https = require('https');
const config = require('./config.json');

exports.handler = (event, context, callback) => {
    console.log('Received event: ', JSON.stringify(event, null, 4));
    let msg = JSON.parse(event.Records[0].Sns.Message);

    const payload = JSON.stringify({
        text: '<!channel> There is a ' + msg.approval.stageName + ' ready for approval: ' + msg.approval.approvalReviewLink,
        attachments: [
            {
                fallback: '<!channel> There is a ' + msg.approval.stageName + ' ready for approval: ' + msg.approval.approvalReviewLink,
                actions: [
                    {
                        type: "button",
                        text: "View " + msg.approval.stageName + " Approval",
                        url: msg.approval.approvalReviewLink
                    }
                ],
                color: "good"
            }
        ]
    });

    const options = {
        hostname: "hooks.slack.com",
        method: "POST",
        path: (config && config.path) || "/services/T5H1Z1LRE/BDB6GE8GN/4wA61eMeZxjEk8GudvAMjc9T"
    };

    const req = https.request(options, (res) => res.on("data", () => callback(null, "OK")));
    req.on("error", (error) => callback(JSON.stringify(error)));
    req.write(payload);
    req.end();
}
