# frozen_string_literal: true

require 'cfndsl'

CloudFormation do
    params = {}
    external_parameters.each_pair do |key, val|
        key = key.to_sym
        params[key] = val
    end
    Description 'IAM Roles & Policies to enable CodePipeline deployments in this account'

    # Parameter (prefix)
    Parameter(:prefix) do
        Type String
        AllowedPattern '[a-z]*[-a-z0-9]*'
        ConstraintDescription 'A lower case string between 3 and 16 characters'
        Description 'A lower case string between 3 and 16 characters'
        MinLength 3
        MaxLength 16
    end

    # Parameter (deployenv)
    Parameter(:deployenv) do
        Type String
        AllowedPattern '[a-z]*[-a-z0-9]*'
        ConstraintDescription 'A lower case string between 3 and 16 characters'
        Description 'A lower case string between 3 and 16 characters'
        MinLength 3
        MaxLength 16
    end

    # Parameter (deployenv)
    Parameter(:reprojectimage) do
        Type String
        AllowedPattern '[a-z]*[-a-z0-9]*'
        ConstraintDescription 'A lower case string between 3 and 16 characters'
        Description 'A lower case string between 3 and 16 characters'
        MinLength 3
        MaxLength 16
    end

    # Resource Conditions - used to limit resource creation on deployenv
    Condition(:onlyDev, FnEquals(Ref(:deployenv), 'dev'))

    # IAM Policy (CodePipeline/Cloudformation)
    IAM_Policy(:codepipelinecloudformationpolicy) do
        PolicyName FnJoin('', [Ref(:prefix), '-codepipelinecloudformation'])
        PolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Action: [
                        'cloudformation:*',
                        's3:*',
                        'iam:PassRole'
                    ],
                    Resource: '*'
                },
                {
                    Effect: 'Allow',
                    Action: [
                        'kms:*'
                    ],
                    Resource: '*'
                }
            ]
        )
        Roles [Ref(:codepipelinecloudformationrole)]
    end

    # IAM Role (CodePipeline/Cloudformation)
    IAM_Role(:codepipelinecloudformationrole) do
        RoleName FnJoin('', [Ref(:prefix), '-codepipelinecloudformationrole'])
        AssumeRolePolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: [
                        'sts:AssumeRole'
                    ],
                    Effect: 'Allow',
                    Principal: {
                        AWS: "arn:aws:iam::#{params[:accountids]['development']}:root"
                    }
                }
            ]
        )
    end

    # IAM Policy (Cloudformation Deploy)
    IAM_Policy(:cloudformationdeploypolicy) do
        PolicyName FnJoin('', [Ref(:prefix), '-cloudformationdeploy'])
        PolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Action: [
                        '*',
                        'iam:PassRole'
                    ],
                    Resource: '*'
                },
                {
                    Effect: 'Allow',
                    Action: [
                        's3:PutObject',
                        's3:GetBucketPolicy',
                        's3:GetObject',
                        's3:ListBucket'
                    ],
                    Resource: '*'
                }
            ]
        )
        Roles [Ref(:cloudformationdeployrole)]
    end

    # IAM Role (Cloudformation Deploy)
    IAM_Role(:cloudformationdeployrole) do
        RoleName FnJoin('', [Ref(:prefix), '-cloudformationdeployrole'])
        AssumeRolePolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: [
                        'sts:AssumeRole'
                    ],
                    Effect: 'Allow',
                    Principal: {
                        Service: 'cloudformation.amazonaws.com'
                    }
                }
            ]
        )
    end

    # IAM Role (codebuild)
    IAM_Role(:codebuildrole) do
        Condition :onlyDev
        RoleName FnJoin('', [Ref(:prefix), '-codebuild'])
        AssumeRolePolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: [
                        'sts:AssumeRole'
                    ],
                    Effect: 'Allow',
                    Principal: {
                        Service: 'codebuild.amazonaws.com'
                    }
                }
            ]
        )
        Policies(
            [
                PolicyName: FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'codebuild-policy']),
                PolicyDocument: {
                    Version: '2012-10-17',
                    Statement: [
                        {
                            Effect: 'Allow',
                            Action: [
                                'kms:ListKeys',
                                'kms:ListAliases'
                            ],
                            Resource: '*'
                        },
                        {
                            Effect: 'Allow',
                            Action: [
                                's3:PutObject'
                            ],
                            Resource: '*'
                        },
                        {
                            Effect: 'Allow',
                            Action: [
                                'ecr:*'
                            ],
                            Resource: '*'
                        },
                        {
                            Action: [
                                'sts:AssumeRole'
                            ],
                            Effect: 'Allow',
                            Resource: [
                                FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-deployoutcodebuild']),
                                FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-deployoutcodebuild']),
                                FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-deployoutcodebuild'])
                            ]
                        }
                    ]
                }
            ]
        )
    end

    # IAM Role (Deployout Codebuild)
    IAM_Role(:deployoutcodebuildrole) do
        params[:deployenv] == 'dev' && (DependsOn :codebuildrole)
        RoleName FnJoin('', [Ref(:prefix), '-deployoutcodebuild'])
        AssumeRolePolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: [
                        'sts:AssumeRole'
                    ],
                    Effect: 'Allow',
                    Principal: {
                        AWS: FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codebuild'])
                    }
                }
            ]
        )
        Policies(
            [
                PolicyName: FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'deployoutcodebuild-policy']),
                PolicyDocument: {
                    Version: '2012-10-17',
                    Statement: [
                        {
                            Effect: 'Allow',
                            Action: [
                                'cloudformation:Describe*'
                            ],
                            Resource: '*'
                        }
                    ]
                }
            ]
        )
    end

    # IAM Role (codepipeline)
    IAM_Role(:codepipelinerole) do
        Condition :onlyDev
        RoleName FnJoin('', [Ref(:prefix), '-codepipeline'])
        AssumeRolePolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: [
                        'sts:AssumeRole'
                    ],
                    Effect: 'Allow',
                    Principal: {
                        Service: 'codepipeline.amazonaws.com'
                    }
                }
            ]
        )
        Policies(
            [
                PolicyName: FnJoin('-', [Ref(:prefix), '-codepipeline-policy']),
                PolicyDocument: {
                    Version: '2012-10-17',
                    Statement: [
                        {
                            Action: [
                                'sts:AssumeRole'
                            ],
                            Effect: 'Allow',
                            Resource: '*'
                        }
                    ]
                }
            ]
        )
    end

    # IAM Role - Execution role for Lambda functions executed by Codepipeline
    IAM_Role(:codepipelinelambdaexecutionrole) do
        Condition :onlyDev
        RoleName FnJoin('-', [Ref(:prefix), 'codepipeline-lambda'])
        AssumeRolePolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: ['sts:AssumeRole'],
                    Effect: 'Allow',
                    Principal: {
                        Service: 'lambda.amazonaws.com'
                    }
                }
            ]
        )
        ManagedPolicyArns(
            [
                'arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole'
            ]
        )
        Policies(
            [
                PolicyName: FnJoin('-', [Ref(:prefix), 'codepipeline-lambda-policy']),
                PolicyDocument: {
                    Version: '2012-10-17',
                    Statement: [
                        {
                            Action: [
                                'codepipeline:PutJobSuccessResult',
                                'codepipeline:PutJobFailureResult'
                            ],
                            Effect: 'Allow',
                            Resource: '*'
                        },
                        {
                            Action: [
                                'sts:AssumeRole'
                            ],
                            Effect: 'Allow',
                            Resource: '*'
                        }
                    ]
                }
            ]
        )
    end

    # Lambda Cross-Account Execution Role - used by Codepipeline Lambda functions to assume roles in test/prod accounts
    IAM_Role(:pipelinelambdarole) do
        params[:deployenv] == 'dev' && (DependsOn :codepipelinelambdaexecutionrole)
        RoleName FnJoin('', [Ref(:prefix), '-lambdaexecution'])
        AssumeRolePolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: [
                        'sts:AssumeRole'
                    ],
                    Effect: 'Allow',
                    Principal: {
                        "AWS": FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codepipeline-lambda'])
                    }
                }
            ]
        )
        Policies(
            [
                PolicyName: FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'pipelinelambdaexecutionpolicy']),
                PolicyDocument: {
                    Version: '2012-10-17',
                    Statement: [
                        {
                            Effect: 'Allow',
                            Action: [
                                'cloudfront:CreateInvalidation'
                            ],
                            Resource: '*'
                        },
                        {
                            Effect: 'Allow',
                            Action: [
                                'cloudformation:Describe*'
                            ],
                            Resource: '*'
                        }
                    ]
                }
            ]
        )
    end

    # IAM Role - Cross account S3 deploy role
    IAM_Role(:s3deployrole) do
        params[:deployenv] == 'dev' && (DependsOn :codepipelinerole)
        RoleName FnJoin('-', [Ref(:prefix), 's3deploy'])
        AssumeRolePolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: ['sts:AssumeRole'],
                    Effect: 'Allow',
                    Principal: {
                        AWS: FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codepipeline'])
                    }
                }
            ]
        )
        Policies(
            [
                PolicyName: FnJoin('-', [Ref(:prefix), 's3deploy-policy']),
                PolicyDocument: {
                    Version: '2012-10-17',
                    Statement: [
                        {
                            Effect: 'Allow',
                            Action: [
                                's3:PutObject',
                                's3:GetObject'
                            ],
                            Resource: '*'
                        },
                        {
                            Effect: 'Allow',
                            Action: [
                                'kms:Encrypt',
                                'kms:Decrypt',
                                'kms:ReEncrypt*',
                                'kms:GenerateDataKey*',
                                'kms:DescribeKey'
                            ],
                            Resource: '*'
                        }
                    ]
                }
            ]
        )
    end

    # IAM Group - Bitbucket
    IAM_Group(:bitbucketgroup) do
        GroupName FnJoin('', [Ref(:prefix), '-bitbucket'])
    end

    # IAM User - Bitbucket
    IAM_User(:bitbucketuser) do
        UserName FnJoin('', [Ref(:prefix), '-bitbucket'])
        Groups [Ref(:bitbucketgroup)]
    end

    # IAM Policy - Bitbucket
    IAM_Policy(:bitbucketpolicy) do
        PolicyName FnJoin('', [Ref(:prefix), '-bitbucket'])
        Groups [Ref(:bitbucketgroup)]
        PolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Resource: '*',
                    Action: [
                        'ecr:GetAuthorizationToken',
                        'ecr:CreateRepository',
                        'ecr:DescribeRepositories',
                        'ecr:GetDownloadUrlForLayer',
                        'ecr:BatchGetImage',
                        'ecr:BatchCheckLayerAvailability',
                        'ecr:CompleteLayerUpload',
                        'ecr:InitiateLayerUpload',
                        'ecr:PutImage',
                        'ecr:UploadLayerPart'
                    ]
                },
                {
                    Effect: 'Allow',
                    Resource: FnJoin('', ['arn:aws:ecr:', Ref('AWS::Region'), ':', Ref('AWS::AccountId'), ':', 'repository', '/', Ref(:prefix)]),
                    Action: [
                        'ecr:DescribeRepositories',
                        'ecr:GetDownloadUrlForLayer',
                        'ecr:BatchGetImage'
                    ]
                },
                {
                    Effect: 'Allow',
                    Resource: FnJoin('', ['arn:aws:ecr:', Ref('AWS::Region'), ':', Ref('AWS::AccountId'), ':', 'repository', '/', Ref(:deployenv), '-', Ref(:reprojectimage)]),
                    Action: [
                        'ecr:DescribeRepositories',
                        'ecr:GetDownloadUrlForLayer',
                        'ecr:BatchGetImage',
                        'ecr:BatchCheckLayerAvailability',
                        'ecr:CompleteLayerUpload',
                        'ecr:InitiateLayerUpload',
                        'ecr:PutImage',
                        'ecr:UploadLayerPart'
                    ]
                }
            ]
        )
    end

    # IAM Group - gisdata user
    IAM_Group(:gisdatagroup) do
        GroupName FnJoin('', [Ref(:prefix), '-gisdata'])
    end

    # IAM User - gisdata user
    IAM_User(:gisdatauser) do
        UserName FnJoin('', [Ref(:prefix), '-gisdata'])
        Groups [Ref(:gisdatagroup)]
    end

    # IAM Policy - gisdata user
    IAM_Policy(:gisdatapolicy) do
        PolicyName FnJoin('', [Ref(:prefix), '-gisdata'])
        Groups [Ref(:gisdatagroup)]
        PolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Resource: '*',
                    Action: [
                        's3:*'
                    ]
                }
            ]
        )
    end
end
