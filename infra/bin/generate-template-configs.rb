#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'
require 'json'

CONFIG = YAML.load_file('config/app.yml').freeze
OUTPUTDIR = ENV['OUTPUTDIR']

tags = CONFIG['tags']

%w[Development Testing Production].each do |stage|
    tags['TIER'] = stage
    template_config = { Parameters: {}, Tags: tags, StackPolicy: { Statement: [] } }
    fh = File.open("#{OUTPUTDIR}/#{stage}-config.json", 'w')
    fh.puts JSON.pretty_generate template_config
    fh.close
end
