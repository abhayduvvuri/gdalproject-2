mkdir -p ./tmp
zip -r9 ./tmp/<artifact>.zip .
export AWS_ACCESS_KEY_ID=<ACCESS_KEY_ID>
export AWS_SECRET_ACCESS_KEY=<ACCESS_KEY>
export AWS_DEFAULT_REGION=ap-southeast-2
aws s3 cp ./tmp/<artifact>.zip s3://<bucketname>/
