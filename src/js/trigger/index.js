"use strict";
var AWS = require("aws-sdk");
AWS.config.update({ region: "ap-southeast-2" });
var ecs = new AWS.ECS({ apiVersion: "2014-11-13" });
exports.handler = function (event) {

    const prefix = process.env.prefix;
    const deployenv = process.env.deployenv;
    const subnetA = process.env.subnetA;
    const subnetB = process.env.subnetB;
    const svcName = process.env.svcName;
    const clusterName = `${prefix}-${deployenv}-${svcName}-Cluster`;
    const tskDef = `${prefix}-${deployenv}-${svcName}-TaskDefinition`;

    const bucket = event.Records[0].s3.bucket.name;
    const key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " "));
    let keyName = key.split(".")[0];
    let filename = keyName.split("/").pop()
    
    let newKey = event.Records[0].s3.object.key;
    let src_folder = `s3://${bucket}/` + newKey.substring(0, newKey.lastIndexOf("/")+1);
    src_folder = src_folder.slice(0, -1);
    let dest_folder = src_folder.replace('gis-data','gis-projected-data');

    try {
        console.log(`${bucket}${key}`);
    } catch (err) {
        console.log(err);
        const message = `Error getting object ${key} from bucket ${bucket}. Make sure they exist and your bucket is in the same region as this function.`;
        console.log(message);
        throw new Error(message);
    }

    try {
        // run an ECS Fargate task
        const params = {
            cluster: clusterName,
            launchType: "FARGATE",
            taskDefinition: tskDef,
            count: 1,
            platformVersion: "LATEST",
            networkConfiguration: {
                awsvpcConfiguration: {
                    subnets: [
                        subnetA, subnetB
                    ],
                    assignPublicIp: "ENABLED"
                }
            },
            overrides: {
                containerOverrides: [{
                    name: svcName,
                    environment: [{
                        name: "PREFIX",
                        value: prefix
                    },
                    {
                        name: "DEPLOYENV",
                        value: deployenv
                    },
                    {
                        name: "FILENAME",
                        value: filename
                    },
                    {
                        name: "SRC_FOLDER",
                        value: src_folder
                    },
                    {
                        name: "DEST_FOLDER",
                        value: dest_folder
                    }]
                }]
            }
        };
        ecs.runTask(params, function (err, data) {
            if (err) { console.warn("error: ", "Error while starting task: " + err); }
            else { console.info("Task started: " + JSON.stringify(data.tasks)); }
        });
    }
    catch (err) {
        console.log(err);
        const message = "Error ecs";
        console.log(message);
        throw new Error(message);
    }
};