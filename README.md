# GDAL AWS Infra
This repository contains a sample GDAL transformation app, and a full AWS infra pipeline setup. Can be used as a starting point for any AWS native applications.

Repo includes infra code to bootstrap accounts (set up ECR repo for Docker container image), create automated deployment pipeline (3 tiers in seperate accounts), and a basic GDAL Project API gateway endpoint.

Use this repo in conjunction with the reference guide here :
http://issuwiki/display/IP/AWS+Infra+Deployments

## Modules
Source code is divided into the following modules:
1. src - Application code for the GDAL transformation app. Includes Lambda functions invoked by calls to the serverless API defined in infra code.
2. infra - Application & deployment pipeline infrastructure code
